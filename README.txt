Lex.java: Recieves two command line arguments, exits the program if more than two are recieved. The first argument (args[0])
	  is used as an input file that Strings will be read from, while the second argument (args[1]) is used as an output file.
	  Reads all lines in from args[0] and stores them in a String array. The indices of each String in the array
	  are passed into a List object one by one from List.java in alphabetic order. This set of ordered indices
	  is then used to send elements from the String array to the output file in the correct alphabetic order.
List.java: My List ADT implementation that allows for traversal forwards and backwards. Contains a private nested Node class
	   that holds an int value in its data field, and a refrence to the next node, and previous node. This List ADT is used
	   for holding the sorted int indices passed to it from Lex.java.
ListClient.java: Provided file used for testing the functionality of List.java and ensuring it works as intended.
Makefile.txt: Standard Makefile that was provided.
README.txt: This file, serves as "table of contents" for files provided.